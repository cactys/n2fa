#!/usr/bin/env node

const chalk = require('chalk')
	, fs = require('fs')
	, otp = require('otplib')
	, path = require('path')
	, readline = require('readline')
	, table = require('table')

// #### App constants ####

	, gaugeSize = 0.5
	, periodLength = 30
	, tableConfig = {
		border: {
			topBody: chalk.gray(`─`),
			topJoin: chalk.gray(`┬`),
			topLeft: chalk.gray(`┌`),
			topRight: chalk.gray(`┐`),

			bottomBody: chalk.gray(`─`),
			bottomJoin: chalk.gray(`┴`),
			bottomLeft: chalk.gray(`└`),
			bottomRight: chalk.gray(`┘`),

			bodyLeft: chalk.gray(`│`),
			bodyRight: chalk.gray(`│`),
			bodyJoin: chalk.gray(`│`),

			joinBody: chalk.gray(`─`),
			joinLeft: chalk.gray(`├`),
			joinRight: chalk.gray(`┤`),
			joinJoin: chalk.gray(`┼`)
		}
	}


const shizzle = configPath => {
	process.stdout.write('\033[?1049h\033[H')

	readline.emitKeypressEvents(process.stdin)
	process.stdin.setRawMode(true)
	process.stdin.on('keypress', str => {
		if(str && str.toLowerCase() === 'q') {
			process.stdout.write('\033[?1049l')
			process.exit(0)
		}
	})

	nextRender = new Date(new Date().getTime())
	nextRender.setMilliseconds(0)
	nextRender.setSeconds(0)

	setInterval(function() {
		render(configPath)
	}, 20)
}

let data
let nextRender

const render = configPath => {
	if(!data) {
		data = require(configPath)
	}

	if(new Date() > nextRender) {
		const tokenTable = [[
				chalk.underline('Service'),
				chalk.underline('Account'),
				chalk.underline('Token'),
			]]


		for(let entry of data) {
			let first = true
			let odd = true
			for(let account of Object.keys(entry.accounts)) {
				const code = otp.authenticator.generate(entry.accounts[account].replace(/[ ]/g, ''))
					, row = [
						first ? chalk.magenta(entry.service) : '',
						(odd ? chalk.green : chalk.yellow)(account),
						(odd ? chalk.green : chalk.yellow)(code)
					]

				tokenTable.push(row)
				first = false
				odd = !odd
			}
		}

		process.stdout.write('\033[2J\033[1;1H\n\n'
			+ table.table(tokenTable, tableConfig).replace(/│([^\n]+)\n[^\n]+\n([^│]+)│[^ ]+  /g, '│$1\n$2│  ')
			+ '\n'
			+ chalk.red(`Hit 'Q' to exit.`)
		)

		nextRender = new Date(nextRender.getTime() + 60000)
	}

	process.stdout.write('\033[0;0H')
	renderGauge()
}

const renderGauge = () => {
	let d = new Date()

	let full = (d.getSeconds() < 30 ? '▓' : '░').repeat(d.getSeconds() % periodLength)
	let partial = d.getMilliseconds() >= 500 ? '▒' : ''

	let gauge = full + partial
	let empty = (d.getSeconds() < 30 ? '░' : '▓').repeat(periodLength - gauge.length)

	gauge = (full + '' ) + partial + empty

	process.stdout.write(gauge + ' ' + ('0' + (periodLength - (d.getSeconds() % periodLength))).substr(-2) + 's\r')
}

if(!module.parent) {
    if(process.argv[2]) {
        let configPath = process.argv[2]
        if(fs.existsSync(configPath)) {
            if(!path.isAbsolute(configPath)) {
                configPath = `./${configPath}`
            }
            shizzle(configPath)
            return
        } else {
            console.error(chalk.red(`Given config file "${configPath}" does not exist!`))
        }
    } else {
        console.error(chalk.red('No config file given!'))
    }

    console.log(`Usage: ${chalk.cyan('n2fa')} <${chalk.yellow('configFile')}>`)
}
